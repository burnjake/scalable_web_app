# Scalable Web App

## Setting Up
**Install Terraform locally** <br>
https://learn.hashicorp.com/terraform/getting-started/install <br>
On macOS, you can use brew: "brew install terraform". This will also add Terraform to your path.

**Clone this repo** <br>
git clone git@gitlab.com:burnjake/scalable_web_app.git

**Configure variables** <br>
Replace placeholder values in terraform/terraform.tfvars file. <br>

*secret_key*: AWS secret key value. <br>
*access_key*: AWS access key value. <br>
*local_cidr*: CIDR range that contains the IP of the machine that's executing this Terraform. e.g. 172.10.10.10/32. <br>
*vpc_id*: id of the AWS VPC that's being deployed to. <br>
*subnet_id*: id of the AWS subnet that being deployed to. <br>
*private_key_path*: Local path of private key for ec2-user user. Used to provison file to ec2 instances. <br>
*public_key*: Value of public key for aws key pair. <br>

## How to browse <br>
Navigate to the public DNS of the elb in your browser on port 80. <br>

## Architecture <br>
![Architecture](web_app.png)


## Scalability <br>
It is possible to scale both horizontally and vertically by changing certain parameters in the **terraform/web_app_ec2.tf** terraform file. <br>

**Vertical** <br>
Change the instance_type variable to a type of your choice (https://aws.amazon.com/ec2/instance-types/). It is possible to decrease or increase the compute. <br>

**Horizontal** <br>
Change the count variable to reflect the desired number of nodes. The elb will automatically pick up any changes to the number of nodes and route traffic accordingly. <br>

**Deploying changes** <br>
Navigate to the scalable_web_app/terraform folder in your terminal and run the following: <br>
1.  Perform a basic syntax check on the terraform: "terraform validate"
2.  Produce an execution plan: "terraform plan"
3.  If happy with the plan, apply the new changes: "terraform apply". Type "yes" when prompted.



