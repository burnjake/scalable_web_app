resource "aws_security_group" "web_app_elb" {
    name        = "web_app_elb"
    description = "Allow client connections to elb"
    vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group_rule" "ingress_client_to_elb" {
    type = "ingress"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks = ["${var.local_cidr}"]
    security_group_id = "${aws_security_group.web_app_elb.id}"
}

resource "aws_security_group_rule" "egress_elb_to_ec2" {
    type = "egress"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.web_app.id}"
    security_group_id = "${aws_security_group.web_app_elb.id}"
}