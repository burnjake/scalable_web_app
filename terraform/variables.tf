variable "secret_key" {}
variable "access_key" {}
variable "local_cidr" {}
variable "vpc_id" {}
variable "subnet_id" {}
variable "private_key_path" {}
variable "public_key" {}

