#!/bin/bash

yum install -q -y httpd >> /tmp/install_status
mv /home/ec2-user/image.png /var/www/html/image.png

cat << EOF > /var/www/html/index.html
<!DOCTYPE html>
<html>
<body>

<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip ex ea commodo consequat. Duis aute 
irure dolor in reprehenderit in voluptate velit esse cillum 
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
non proident, sunt in culpa qui officia deserunt mollit anim
id est laborum.
</p>

<img src="image.png" width="500" height="300">

</body>
</html>
EOF

systemctl enable httpd >> /tmp/install_status
systemctl start httpd >> /tmp/install_status