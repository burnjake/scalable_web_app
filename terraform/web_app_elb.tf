resource "aws_elb" "web_app_elb" {
    name = "web-app-elb"
    subnets = ["${var.subnet_id}"]
    security_groups = ["${aws_security_group.web_app_elb.id}"]
    instances = ["${aws_instance.web_app.*.id}"]

    listener {
        instance_port     = 80
        instance_protocol = "http"
        lb_port           = 80
        lb_protocol       = "http"
    }

    health_check {
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout             = 3
        target              = "HTTP:80/"
        interval            = 30
    }
}
