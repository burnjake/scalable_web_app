provider "aws" {
    region                  = "eu-west-2"
    profile                 = "default"
    access_key              = "${var.access_key}"
    secret_key              = "${var.secret_key}"
    version                 = "~> 2.0"
}