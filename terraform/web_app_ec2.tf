resource "aws_instance" "web_app" {
    count                   = 3
    ami                     = "ami-07a5200f3fa9c33d3"
    instance_type           = "t2.micro"
    subnet_id               = "${var.subnet_id}"
    vpc_security_group_ids  = ["${aws_security_group.web_app.id}"]
    key_name                = "${aws_key_pair.ec2-user.key_name}"
    user_data               = "${data.template_file.web_app_user_data.rendered}"

    tags = {
        Name = "web_app_${count.index + 1}"
        Terraform = "true"
    }

    root_block_device {
        volume_size = 10
    }

    provisioner "file" {
        source      = "resources/image.png"
        destination = "/home/ec2-user/image.png"

        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = "${file("${var.private_key_path}")}"
        }
    }
}