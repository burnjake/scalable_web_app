resource "aws_security_group" "web_app" {
    name        = "web_app"
    description = "Allow inbound SSH/connection to web app ports"
    vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group_rule" "ingress_allow_ssh" {
    type = "ingress"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = ["${var.local_cidr}"]
    security_group_id = "${aws_security_group.web_app.id}"
}

resource "aws_security_group_rule" "ingress_elb_to_ec2" {
    type = "ingress"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.web_app_elb.id}"
    security_group_id = "${aws_security_group.web_app.id}"
}

resource "aws_security_group_rule" "egress_allow_all" {
    type = "egress"
    from_port       = 0
    to_port         = 0
    protocol        = -1
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.web_app.id}"
}
