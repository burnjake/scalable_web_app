resource "aws_key_pair" "ec2-user" {
    key_name   = "ec2-user"
    public_key = "${var.public_key}"
}